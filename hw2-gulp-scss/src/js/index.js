const burger = document.querySelector(".header__menu-burger_btn");
const menu = document.querySelector(".header__menu");

burger.addEventListener("click", () => {
  burger.classList.toggle("active");
  menu.classList.toggle("open");
});

window.addEventListener("resize", () => {
  const screenWidthElement = window.screen.width;
  if (screenWidthElement > 768) {
    burger.classList.remove("active");
    menu.classList.remove("open");
  }
});

window.addEventListener("click", (event) => {
  if (
    event.target.closest(".header__menu-burger_btn") === null &&
    event.target.closest(".header__menu") === null
  ) {
    burger.classList.remove("active");
    menu.classList.remove("open");
  }
});
