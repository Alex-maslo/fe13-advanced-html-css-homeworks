const burger = document.querySelector(".header__navigation-burger-button");
const dropMenu = document.querySelector(".header__drop-menu");
const dropMenuItem = document.querySelector(".header__drop-menu--text");

burger.addEventListener("click", () => {
  burger.classList.toggle("active");
  dropMenu.classList.toggle("open");
});

window.addEventListener("click", (event) => {
  if (
    event.target.closest(".header__navigation-burger-button") === null &&
    event.target.closest(".header__drop-menu") === null
  ) {
    burger.classList.remove("active");
    dropMenu.classList.remove("open");
  }
});

window.addEventListener("resize", () => {
  const screenWidthElement = window.screen.width;
  if (screenWidthElement > 480) {
    burger.classList.remove("active");
    dropMenu.classList.remove("open");
  }
});
