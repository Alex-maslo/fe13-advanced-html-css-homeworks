import browserSync from "browser-sync";
import gulp from "gulp";
import autoprefixer from "gulp-autoprefixer";
import clean from "gulp-clean";
import cleanCSS from "gulp-clean-css";
import concat from "gulp-concat";
import cssnano from "gulp-cssnano";
import dartSass from "sass";
import gulpSass from "gulp-sass";
import rename from "gulp-rename";
import uglify from "gulp-uglify";
import minifyjs from "gulp-js-minify";
import imagemin from "gulp-imagemin";

const sass = gulpSass(dartSass);
const bsSync = browserSync.create();

const { parallel, watch, series, src, dest } = gulp;

function clear() {
  return src("./dist/*", {
    read: false,
  }).pipe(clean());
}

function styles() {
  return src("./src/scss/*.*")
    .pipe(sass())
    .pipe(
      autoprefixer({
        overrideBrowserslist: ["last 2 versions"],
        cascade: false,
      })
    )
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(
      rename({
        basename: "style",
        extname: ".min.css",
      })
    )
    .pipe(cssnano())
    .pipe(dest("./dist/css/"))
    .pipe(bsSync.reload({ stream: true }));
}

function scripts() {
  return src("./src/js/*.js")
    .pipe(concat("scripts.js"))
    .pipe(uglify())
    .pipe(minifyjs())

    .pipe(
      rename({
        extname: ".min.js",
      })
    )
    .pipe(dest("./dist/js/"))
    .pipe(bsSync.reload({ stream: true }));
}

function img() {
  return src("src/img/**/*.*")
    .pipe(imagemin())
    .pipe(dest("dist/img"))
    .pipe(bsSync.reload({ stream: true }));
}

function server() {
  return bsSync.init({
    server: {
      baseDir: "./",
    },
  });
}

function watcher() {
  watch("./src/scss/*.scss", styles);
  watch("./src/js/*.js", scripts);
  watch("./src/img/*.*", img);
  watch("./index.html").on("change", bsSync.reload);
}

export const build = series(clear, img, styles, scripts);
export const dev = parallel(server, watcher);
export default dev;
